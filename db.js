var mongoose   = require('mongoose');
mongoose.Promise = require('bluebird');

var uri = 'mongodb://root:root@test-shard-00-00-asy9t.mongodb.net:27017,test-shard-00-01-asy9t.mongodb.net:27017,test-shard-00-02-asy9t.mongodb.net:27017/poll?ssl=true&replicaSet=Test-shard-0&authSource=admin'
exports.connect = function(){
    mongoose.connect(uri, { useMongoClient: true });
}

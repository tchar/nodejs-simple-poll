var Poll = require('./models/poll.js');

exports.getPoll = function(req, res){
  	res.render('index');
}

exports.postPoll = function(req, res){
	var poll = new Poll();
	poll.question = req.body.question;
	poll.choices = req.body.choices;
	poll.type = req.body.poll_type;

	var promise = poll.save();
	promise.then(function(doc) {
		res.json(doc);
	}).catch(function(err){
		console.log(err);
	})
}

exports.vote = function(req, res){
	var id = req.params.poll_id;
	var prom = Poll.findById(req.params.poll_id);

	prom.then(function(doc){
		res.render('vote', {poll: doc});
	}).catch(function(err){
		res.status(404).send();
	});
}

exports.postVote = function(req, res){
	var prom = Poll.findById(req.params.poll_id);

	prom.then(function(doc){
		try {
			if (doc.type == doc.TYPE_MULTI){
				if (req.body.choices.constructor == Array){
					var arr = new Set(req.body.choices);
					arr.forEach(function(e){
						var val = doc.votes[e] + 1;
						doc.votes.set(e, val);
					});
				} else {
					var val = doc.votes[req.body.choices] + 1;
					doc.votes.set(req.body.choices, val);
				}
			} else if (doc.type == doc.TYPE_SINGLE){
				var val = doc.votes[req.body.choices] + 1;
				doc.votes.set(req.body.choices, val);
			}
			var promSave = doc.save();
			promSave.then(function(dd){
				res.json({});
			}).catch(function(error){
				console.log(error);
				res.status(500).send();
			})
		} catch (err){
			console.log(err);
			res.status(500).send();
		}
		
	}).catch(function(err){
		res.status(404).send();
	})
}

exports.results = function(req, res){
	var id = req.params.poll_id;
	var prom = Poll.findById(req.params.poll_id);

	prom.then(function(doc){
		res.render('results', {poll: doc});
	}).catch(function(err){
		res.status(404).send();
	});
}
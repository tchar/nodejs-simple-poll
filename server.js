var express 		= require('express');
var app 			= express();
var bodyParser		= require('body-parser');
var db 				= require('./db.js');
var sass 			= require('node-sass');
var sassMiddleware 	= require('node-sass-middleware');
var path 			= require('path');
var pollCtrl 		= require('./poll_controller.js')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

db.connect();

var srcPath = path.join(__dirname, '/static/sass');
var destPath =path.join( __dirname, '/static/css');
app.use(
     sassMiddleware({
         src: srcPath, 
         dest: destPath,
         debug: true, 
         prefix: '/css'      
     })
  ); 

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
app.get('/', pollCtrl.getPoll);
app.post('/', pollCtrl.postPoll);

app.get('/:poll_id/results', pollCtrl.results);
app.get('/:poll_id', pollCtrl.vote);
app.post('/:poll_id', pollCtrl.postVote);

app.use(express.static(path.join(__dirname, '/static')));

var port = process.env.PORT || 8080;

app.listen(port);
console.log('Listening...');
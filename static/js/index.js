contextObj = {  
                counter: 1, 
                limit: 30, 
                initialInputs: 4
};

function pxToRem(px){
    var baseline_px = 16;
    var baseline_rem = baseline_px;
    return px/baseline_rem + 'rem';
}

// Function to style document and prepare it for use
function styleDocument(){
    // Hide no-js class elements
    $('.no-js').css('display', 'none');
    // Change letter spacing to 1px
    $('*').filter(function(){
        return $(this).css('letter-spacing') == '-1px';
    }).css('letter-spacing', '1px').css('letter-spacing', pxToRem(1));
    $('[required]').removeAttr('required').removeAttr('title');
    $('[pattern]').removeAttr('pattern');
    $("[name='choices']").last()
            .on('input', addInput)
            .on('paste', addInput);
    $('#poll-form').attr('action', '#').on('submit', checkSubmit);
    $('#hide-button').on('click', clearAndHide);
    $('#poll-url').on('click', redirectToPoll);
    $('#popup-ok-button').on('click', hidePopup);
    // Add focusout handler to check if input is empty
    $('input').on('focusout', inputComplete)
}

String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};

function inputComplete(e){
    e = e || window.event;
    target = $(e.target || e.srcElement)
    target.val(target.val().trim());
    if (target.val().isEmpty())
        target.val('');
}


// Adds new input to poll-form
function addInput(e){
    e = e || window.event;

    // Do not add more inputs than limit
    if ((contextObj.counter + 
            contextObj.initialInputs) > 
                contextObj.limit)
       return;

    target = $(e.target || e.srcElement)

    // If target's text is empty do not add new input
    if (target.val().isEmpty())
        return;

    // Add new Input
    input = $('<input></input>', {
        type: 'text',
        id: 'i' + (contextObj.counter + contextObj.initialInputs),
        'class': 'choice',
        name: 'choices',
        placeholder: 'Enter an option...',
        maxLength: target.attr('maxLength'),
        autocomplete: 'off',
        on: {
            input: addInput,
            paste: addInput,
            focusout: inputComplete
        }
    });
    input.hide().appendTo('#choices-container').slideDown('fast');
    
    // Remove attached listeners from target
    target.off('input');
    target.off('paste');
    contextObj.counter++;
}

function showPopup(){
    window.scrollTo(0,0);
    $('#fade').css('display', 'inline-block');
    $('#popup-wrapper').css('display', 'inline-block');
}


function hidePopup(){
    $('#popup-wrapper').css('display', 'none');
    $('#fade').css('display', 'none');
}

function clearAndHide(){
    $("#poll-form")[0].reset();
    hidePopup();
    recaptcha.checkBlocked();
}


function alertUser(msg, error){
    if (!error){
        hideOkButton();
        showXButton();
        showUrl();
    } else {
        showOkButton();
        hideXButton();
        hideUrl();
    }

    $('#popup-content').text(msg);
    showPopup();
}

function showOkButton(){
    $('#popup-ok-button-container').css('display', 'block')
}

function hideOkButton(){
    $('#popup-ok-button-container').css('display', 'none');
}

function showXButton(){
    $('#hide-button-container').css('display', 'block');
}

function hideXButton(){
    $('#hide-button-container').css('display', 'none');
}

function showUrl(){
    $('#poll-url-container').css('display', 'block');
}

function hideUrl(){
    $('#poll-url-container').css('display', 'none');
}


function pollCreated(responseObj){
    var url = responseObj['_id'];
    url = window.location.origin + window.location.pathname + url;
    console.log(url);
    $('#poll-url').text(url).attr('value', url);
    // showUrl();
    msg = 'Your poll has been created. Click the link below to redirect to your poll.';
    alertUser(msg, false);
}

function errorOccured(){
    msg = 'Something went wrong, please refresh the page and try again.';
    alertUser(msg, true);
}


function checkSubmit(){
    var msg = 'Something went wrong, please refresh the page and try again.';

    try{
        question_text = $('#question').val();
        if (question_text.isEmpty()){
            msg = 'Question cannot be blank.';
            throw new Error('No question');
        }


        choices = $("[name='choices'");
        not_empty_choices = 0;
        $("[name='choices'").each(function(){
            if (!this.value.isEmpty()){
                not_empty_choices++;
            }
            if (not_empty_choices >= 2)
                return false;   // break;
        });
        
        if (not_empty_choices < 2){
            msg = 'Please provide at least two options!';
            throw new Error('No choices');
        }

        if (recaptcha.enabled && !recaptcha.filled){
            msg = 'Please fill the captcha!';
            throw new Error('Captcha Error');
        }

        doAjax();
    } catch (e){
        alertUser(msg, true);
        console.log(e.stack)
    } finally {
        // Always return false
        return false;
    }
}


function redirectToPoll(){
    url = $('#poll-url').attr('value');
    if (!url)
        return;
    window.location = url;
}

function captchaLoaded(){
    recaptcha.load();
}

recaptcha = {
    enabled: false,
    loaded: false,
    created: false,
    cid: 0, 
    filled: false,
    sitekey : '6Le8wRgTAAAAALnsnR2Jw7YIPk6jUrPWx-A68Gr3',
    elem: null,
    blocked: false,

    load: function() {
        this.loaded = true;
        this.elem = $('.g-recaptcha');   
        if ($('#blocked').length){
            this.enabled = true;
            this.create();
            this.show();
        }
    },

    create: function() {
        if (this.loaded) {
            this.cid = grecaptcha.render(this.elem[0], 
                    { 
                        'sitekey': this.sitekey,
                        'callback': function(){
                            recaptcha.filled = true;
                        } 
            });
            this.created = true;
        }
    },

    show: function(){
        this.elem.show();
    },

    hide: function() {
        this.elem.hide();
    },

    reset: function(){
        if (this.created){
            grecaptcha.reset(this.cid);
            this.filled = false;
        }
    },

    blocked: function(blocked) {
        if (blocked) {
            this.enabled = true;
        } else {
            this.enabled = false;
        }
    },

    checkBlocked: function() {
        if (this.enabled){
            if (!this.loaded)
                errorOccured();
            else if (!this.created)
                this.create();
            else
                this.reset();
            this.show();
        } else {
            this.reset();
            this.hide();
        }
    }
}

function doAjax(){
    $.ajax({
        type: "POST",
        url: "/",
        dataType: "json",      
        data : $('#poll-form').serializeArray(),
        success: function (response) {
            pollCreated(response);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            errorOccured();
        }
    });
}

// When document is ready.
$(document).ready(styleDocument);
// Function to style document
function styleDocument(){
    // Change letter spacing to 1px
    $('*').filter(function(){
    	return $(this).css('letter-spacing') == '-1px';
    }).css('letter-spacing', '1px');
}

$(document).ready(styleDocument);
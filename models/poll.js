var mongoose 		= require('mongoose');
var Schema = mongoose.Schema;

var PollSchema = new Schema({
	question: String,
	choices: [String],
	type: Number,
	votes: [Number]
});

PollSchema.virtual('TYPE_SINGLE').get(function () {
    return 0;
});

PollSchema.virtual('TYPE_MULTI').get(function () {
    return 1;
});

PollSchema.methods.getPollTypeText = function(){
    if (this.type == 1){
    	return 'Multiple Choice';
    } else{
    	return 'Single Choice';
    }
}

PollSchema.pre('save', function(next){
	if (this.isNew){
		var poll = this;
		poll.question = poll.question.trim();
		var choicesLen = poll.choices.length;
		for (var i = choicesLen - 1; i >= 0; i--){
			poll.choices[i] = poll.choices[i].trim();
			if (poll.choices[i].length == 0){
				poll.choices.splice(i, 1);
			}
		}
		if (poll.question.length == 0 || poll.choices.length < 2){
			var err = new Error('Question cannot be empty');
			next(err);
		}
		if (poll.choices.length < 2){
			var err = new Error('You must provide at least two options');
			next(err);
		}
		poll.votes = new Array(poll.choices.length);
		for (var i = 0; i < poll.choices.length; i++){
			poll.votes[i] = 0;
		}
	}
	next();
})

var Poll = mongoose.model('Poll', PollSchema);

module.exports = Poll;